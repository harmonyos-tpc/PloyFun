package cn.hugeterry.ployfun.utils;

import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

/**
 * Created by hugeterry(http://hugeterry.cn)
 * Date: 16/5/1 16:10
 */
public class ConvertGreyImg {
    /**
     * 将彩色图转换为灰度图
     *
     * @param img 位图
     * @return 返回转换好的位图
     */
    public static PixelMap convertGreyImg(PixelMap img) {
        int width = img.getImageInfo().size.width; // 获取位图的宽
        int height = img.getImageInfo().size.height; // 获取位图的高

        int[] pixels = new int[width * height]; // 通过位图的大小创建像素点数组

        img.readPixels(pixels, 0, width, new Rect(0, 0, width, height));
        int alpha = 0xFF << 24;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int grey = pixels[width * i + j];

                int red = ((grey & 0x00FF0000) >> 16);
                int green = ((grey & 0x0000FF00) >> 8);
                int blue = (grey & 0x000000FF);

                grey = (int) ((float) red * 0.3 + (float) green * 0.59 + (float) blue * 0.11);
                grey = alpha | (grey << 16) | (grey << 8) | grey;
                pixels[width * i + j] = grey;
            }
        }

        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(width, height);
        initializationOptions.pixelFormat = PixelFormat.RGB_565;
        initializationOptions.editable = true;
        PixelMap result = PixelMap.create(initializationOptions);
        result.writePixels(pixels, 0, width, new Rect(0, 0, width, height));
        return result;
    }
}
