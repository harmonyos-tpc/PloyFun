package cn.hugeterry.ployfun.utils;

import cn.hugeterry.ployfun.core.delaunay.Pnt;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;

/**
 * Created by hugeterry(http://hugeterry.cn)
 * Date: 16/5/1 16:07
 */
public class DrawTriangle {
    private static int[] x, y;
    private static Paint p;
    private static Path path;

    public static Paint drawTriangle(Pnt[] polygon, int fillColor, Canvas canvas, ResourceManager res) {
        x = new int[polygon.length];
        y = new int[polygon.length];
        for (int i = 0; i < polygon.length; i++) {
            x[i] = (int) polygon[i].coord(0);
            y[i] = (int) polygon[i].coord(1);
        }
        p = new Paint();
        path = new Path();
        p.setColor(new Color(fillColor));
        path.moveTo(x[0], y[0]);
        path.lineTo(x[1], y[1]);
        path.lineTo(x[2], y[2]);
        path.close();
        canvas.drawPath(path, p);
        return p;
    }
}
