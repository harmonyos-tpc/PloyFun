package cn.hugeterry.ployfun;

import cn.hugeterry.ployfun.core.PolyfunKey;
import cn.hugeterry.ployfun.core.delaunay.Pnt;
import cn.hugeterry.ployfun.core.delaunay.Triangle;
import cn.hugeterry.ployfun.core.delaunay.Triangulation;
import cn.hugeterry.ployfun.model.PointBean;
import cn.hugeterry.ployfun.utils.ConvertGreyImg;
import cn.hugeterry.ployfun.utils.DrawTriangle;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Position;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by hugeterry(http://hugeterry.cn)
 * Date: 16/8/13 11:34
 */
public class StartPolyFun {
    private static final int POLY_START = 1;
    private static final int POLY_STOP = 2;

    public Triangulation dt;
    public List<PointBean> pnts = new ArrayList<>();
    public long time = System.currentTimeMillis();

    private Canvas canvas;
    private PixelMap bmp;

    private int xd, yd;
    private int x, y;
    private int cx, cy, rgb;
    private int in = 0;

    private Context context;
    private CompleteListener completeListener;

    /**
     * create StartPolyFun
     * @param context the context
     * @param pixelMap org pixelMap
     * @param completeListener when complete the listener will callback onComplete
     */
    public StartPolyFun(Context context, PixelMap pixelMap, CompleteListener completeListener) {
        this.context = context;
        this.completeListener = completeListener;
        this.bmp = pixelMap;
    }

    /**
     * start create PixelMap
     */
    public void run() {
        new Thread(
                        () -> {
                            if (completeListener != null) {
                                context.getUITaskDispatcher().asyncDispatch(() -> completeListener.onStart());
                            }
                            // 这是包围三角形的诞生地...
                            int height = bmp.getImageInfo().size.height;
                            int width = bmp.getImageInfo().size.width;

                            Triangle initialTriangle =
                                    new Triangle(
                                            new Pnt(-PolyfunKey.initialSize, -PolyfunKey.initialSize),
                                            new Pnt(PolyfunKey.initialSize + width, -PolyfunKey.initialSize),
                                            new Pnt(width / 2, PolyfunKey.initialSize + height));
                            dt = new Triangulation(initialTriangle);
                            //        bmp = ((MainActivity) context).iv.getDrawingCache();

                            //        Log.i("PolyFun TAG", "height:" + height + ",width:" + width);
                            // 加入四个端点
                            dt.delaunayPlace(new Pnt(1, 1));
                            dt.delaunayPlace(new Pnt(1, height - 1));
                            dt.delaunayPlace(new Pnt(width - 1, 1));
                            dt.delaunayPlace(new Pnt(width - 1, height - 1));
                            // 随机加入一些点
                            Random random = new Random(System.currentTimeMillis());
                            for (int i = 0; i < 50; i++) {
                                int x = random.nextInt(width);
                                int y = random.nextInt(height);
                                dt.delaunayPlace(new Pnt(x, y));
                            }
                            // 灰度取点
                            PixelMap resultBitmap = ConvertGreyImg.convertGreyImg(bmp);
                            canvas = new Canvas(new Texture(bmp));
                            for (int y = 1; y < height - 1; ++y) {
                                for (int x = 1; x < width - 1; ++x) {
                                    int rgb = resultBitmap.readPixel(new Position(x, y));
                                    rgb = (rgb & 0xff0000) >> 16; // 留灰度
                                    if (rgb > PolyfunKey.graMax) {
                                        pnts.add(new PointBean(x, y));
                                    }
                                }
                            }
                            //        Log.i("PolyFun TAG", "未过滤点集有" + pnts.size() + "个，正在随机排序");
                            Collections.shuffle(pnts);
                            int count = Math.min(pnts.size(), PolyfunKey.pc);
                            //        Log.i("PolyFun TAG", "正在加入点并剖分三角，请耐心等待");
                            for (int i = 0; i < count; i++) {
                                PointBean p = pnts.get(i);
                                //  bmp.setPixel(p.x, p.y, 0xffffffff);用来观测加入的三角点
                                dt.delaunayPlace(new Pnt(p.x, p.y));
                            }

                            //        Log.i("PolyFun TAG", "开始绘制最终结果");
                            // 取出所有三角形
                            for (Triangle triangle : dt) {
                                xd = 0;
                                yd = 0;
                                Pnt[] vertices = triangle.toArray(new Pnt[0]); // 取出三个点

                                in = 3;
                                // 判断三个点都在图片内
                                for (Pnt pnt : vertices) {
                                    x = (int) pnt.coord(0);
                                    y = (int) pnt.coord(1);
                                    xd += x;
                                    yd += y;
                                    if (x < 0 || x > width || y < 0 || y > height) {
                                        in -= 1;
                                    }
                                }
                                // 三个点都在图内,才画三角形
                                if (in == 3) {
                                    // 取中点颜色
                                    cx = xd / 3;
                                    cy = yd / 3;
                                    rgb = bmp.readPixel(new Position(cx, cy)); // 三角形填充色
                                    // 绘画图形
                                    DrawTriangle.drawTriangle(vertices, rgb, canvas, context.getResourceManager());
                                }
                            }
                            if (completeListener != null) {
                                context.getUITaskDispatcher().asyncDispatch(() -> completeListener.onComplete(bmp));
                            }
                        })
                .start();
    }

    /**
     * the complete listener
     */
    public interface CompleteListener {
        void onStart();

        void onComplete(PixelMap pixelMap);
    }
}
