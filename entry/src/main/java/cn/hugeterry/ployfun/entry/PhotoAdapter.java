/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.hugeterry.ployfun.entry;

import ohos.aafwk.ability.DataUriUtils;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.util.List;

/**
 * photo adapter
 */
public class PhotoAdapter extends RecycleItemProvider {
    List<Long> ids;
    Context context;
    int width;

    public PhotoAdapter(List<Long> ids, Context context) {
        this.ids = ids;
        this.context = context;
        width =
                context.getResourceManager().getDeviceCapability().width
                        * context.getResourceManager().getDeviceCapability().screenDensity
                        / 160;
    }

    @Override
    public int getCount() {
        if (ids != null) {
            return ids.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (ids != null && i < ids.size()) {
            return ids.get(i);
        }
        else {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout, null, false);
        }

        Image image = (Image) component.findComponentById(ResourceTable.Id_image);

        if (ids != null && ids.size() > i && ids.get(i) != null && ids.get(i) > 0) {
            long id = ids.get(i);
            Uri contentUri = AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
            Uri uri = DataUriUtils.attachId(contentUri, id);
            if (UriUtils.isDataMediaUri(uri)) {
                ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                ImageSource imageSource = UriUtils.uriToImageSource(context, uri);
                PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                image.setPixelMap(pixelMap);
            }
        }
        return component;
    }
}
