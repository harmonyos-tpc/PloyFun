package cn.hugeterry.ployfun.entry;

import cn.hugeterry.ployfun.StartPolyFun;
import cn.hugeterry.ployfun.core.PolyfunKey;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.ability.DataUriUtils;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.bundle.IBundleManager;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.util.*;

public class MainAbility extends Ability {
    static final int PHOTO = 0x01;
    ProgressWheel progressWheel;
    StartPolyFun startPolyFun;
    Image image;
    Text photo;
    Text draw;
    Text back;
    Component photoLayout;
    List<Long> ids;
    ListContainer listContainer;
    Text jd;
    Slider slider;
    PixelMap orgPixelMap;
    PixelMap doPixelMap;

    PhotoAdapter photoAdapter;
    boolean isDraw = false;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        image = (Image) findComponentById(ResourceTable.Id_image);
        photo = (Text) findComponentById(ResourceTable.Id_photo);
        draw = (Text) findComponentById(ResourceTable.Id_draw);
        back = (Text) findComponentById(ResourceTable.Id_back);
        photoLayout = findComponentById(ResourceTable.Id_photolayout);
        progressWheel = (ProgressWheel) findComponentById(ResourceTable.Id_progressWheel);
        Optional<PixelMap> optionalPixelMap = ResUtil.getPixelMap(this, ResourceTable.Media_testimg);
        orgPixelMap = optionalPixelMap.isPresent() ? optionalPixelMap.get() : null;
        image.setPixelMap(orgPixelMap);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_photolist);
        jd = (Text) findComponentById(ResourceTable.Id_jd);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        jd.setText("绘制精度：600");
        slider.setMaxValue(1400);
        slider.setProgressValue(0);
        slider.setValueChangedListener(
                new Slider.ValueChangedListener() {
                    @Override
                    public void onProgressUpdated(Slider slider, int i, boolean b) {
                        PolyfunKey.pc = i + 600;
                        jd.setText("绘制精度：" + PolyfunKey.pc);
                    }

                    @Override
                    public void onTouchStart(Slider slider) {}

                    @Override
                    public void onTouchEnd(Slider slider) {}
                });

        draw.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (orgPixelMap != null && !isDraw) {
                            startPolyFun =
                                    new StartPolyFun(
                                            getContext(),
                                            orgPixelMap,
                                            new StartPolyFun.CompleteListener() {
                                                @Override
                                                public void onStart() {
                                                    isDraw = true;
                                                    progressWheel.setVisibility(Component.VISIBLE);
                                                    progressWheel.spin();
                                                    image.setVisibility(Component.HIDE);
                                                }

                                                @Override
                                                public void onComplete(PixelMap pixelMap) {
                                                    progressWheel.stopSpinning();
                                                    progressWheel.setVisibility(Component.HIDE);
                                                    doPixelMap = pixelMap;
                                                    image.setPixelMap(doPixelMap);
                                                    image.setVisibility(Component.VISIBLE);
                                                    isDraw = false;
                                                }
                                            });
                            startPolyFun.run();
                        }
                    }
                });

        photo.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (photoAdapter == null) {
                            if (verifySelfPermission("ohos.permission.READ_MEDIA")
                                    != IBundleManager.PERMISSION_GRANTED) {
                                // 应用未被授予权限
                                if (canRequestPermission("ohos.permission.READ_MEDIA")) {
                                    // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                                    requestPermissionsFromUser(new String[] {"ohos.permission.READ_MEDIA"}, PHOTO);
                                }
                            } else {
                                // 权限已被授予
                                requestPermissionsFromUser(new String[] {"ohos.permission.READ_MEDIA"}, PHOTO);
                            }
                        } else {
                            showPhoto();
                        }
                    }
                });

        back.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        photoLayout.setVisibility(Component.HIDE);
                    }
                });
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PHOTO: {
                // 匹配requestPermissions的requestCode
                if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    // 权限被授予
                    // 注意：因时间差导致接口权限检查时有无权限，所以对那些因无权限而抛异常的接口进行异常捕获处理
                    DataAbilityHelper helper = DataAbilityHelper.creator(getContext());

                    try {
                        ResultSet resultSet =
                                helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, null, null);
                        resultSet.goToFirstRow();
                        ids = new ArrayList<>();
                        do {
                            long id = resultSet.getLong(resultSet.getColumnIndexForName(AVStorage.AVBaseColumns.ID));
                            if (id > 0) ids.add(id);
                        } while (resultSet.goToNextRow());
                        showPhoto();
                    } catch (DataAbilityRemoteException e) {
                        ids = new ArrayList<>();
                        showPhoto();
                    } catch (Exception e){
                        ids = new ArrayList<>();
                        showPhoto();
                    }
                }
                return;
            }
        }
    }

    private void showPhoto() {
        if (photoAdapter == null) {
            photoAdapter = new PhotoAdapter(ids, getContext());
            listContainer.setItemProvider(photoAdapter);
            listContainer.setItemClickedListener(
                    new ListContainer.ItemClickedListener() {
                        @Override
                        public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                            if (i < ids.size()) {
                                long id = ids.get(i);
                                Uri contentUri = AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
                                Uri uri = DataUriUtils.attachId(contentUri, id);
                                if (UriUtils.isDataMediaUri(uri)) {
                                    ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                                    ImageSource imageSource = UriUtils.uriToImageSource(getContext(), uri);
                                    orgPixelMap = imageSource.createPixelmap(decodingOptions);
                                    image.setPixelMap(orgPixelMap);
                                }
                            }
                            photoLayout.setVisibility(Component.HIDE);
                        }
                    });
        }
        photoLayout.setVisibility(Component.VISIBLE);
    }
}
